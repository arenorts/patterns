<?php
interface Pattern_Mediator_Collegue
{
    public function getValue();
    public function setValue($value);
}