<?php
class Pattern_Adapter_Shop_Api extends Pattern_Adapter_Abstract
{
    protected function _initAdaptee()
    {
        $this->_adaptee = new SoapClient("http://soap.resource.com/table.wsdl");
    }
    
    public function getCatalog()
    {
        $catalog = $this->_adaptee->getCatalog();
        $container = new Pattern_Block_Container();
        foreach ($catalog as $category) {
            $container->add(new Pattern_Block_Text($category['name']));
            $subContainer = new Pattern_Block_Container();
            $container->add($subContainer);
            foreach ($category['items'] as $item) {
                $subContainer->add(new Pattern_Block_Text($item['name']));
                $subContainer->add(new Pattern_Block_Line());
            }
        }
        return $container;
    }  
    
    public function getBalance()
    {
        $balance = $this->_adaptee->getBalance('userName', 'password');
        $container = new Pattern_Block_Container();
        foreach ($balance as $currency => $value) {
            $container->add(new Pattern_Block_Text($currency . ': '));
            $container->add(new Pattern_Block_Text($value . ': '));
            $container->add(new Pattern_Block_Line());
            
        }
        return $container;
    }
}