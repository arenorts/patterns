<?php
abstract class Pattern_Adapter_Abstract
{
    protected $_adaptee;
    
    public function __construct()
    {
        $this->_initAdaptee();
    }
    
    abstract protected function _initAdaptee();
}