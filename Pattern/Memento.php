<?php
class Pattern_Memento
{
	public $savedState;
	
	public function __construct($object) 
	{
		$this->savedState = serialize($object);
	}
	
	public function load()
	{
		return unserialize($this->savedState);
	}
}