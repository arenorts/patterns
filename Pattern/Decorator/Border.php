<?php
class Pattern_Decorator_Border extends Pattern_Decorator_Abstract
{
    protected function _render()
    {
        return '<div style="border:1px solid red">' . parent::_render() . '</div>';
    }
}