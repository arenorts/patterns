<?php
class Pattern_Decorator_Background extends Pattern_Decorator_Abstract
{
    protected function _render()
    {
        return '<div style="background-color:green; padding:10px;">' . parent::_render() . '</div>';
    }
}