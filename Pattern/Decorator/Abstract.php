<?php
abstract class Pattern_Decorator_Abstract extends Pattern_Block_Abstract
{
    protected $_component = null;
    
    public function __construct(Pattern_Block_Abstract $component)
    {
        $this->_component = $component;
    }
 
    protected function getComponent()
    {
        return $this->_component;
    }
 
    protected function _render()
    {
        return $this->getComponent()->render();
    }
    
    public function getChildren()
    {
    	return $this->getComponent()->getChildren();
    }
}