<?php
class Pattern_Block
{
    protected static $_instance;
    protected static $_context = 'simple';
    
    private function __construct()
    {
    }
    
    public static function getInstance()
    {
       if (!isset(self::$_instance)) {
           self::$_instance = new self();
       } 
       return self::$_instance;
    }
    
    public static function setContext($context)
    {
        self::$_context = $context;
    }
    
    public static function getFactory()
    {
        if (self::$_context == 'simple') {
            return new Pattern_Block_Factory_Simple();
        } else {
            return new Pattern_Block_Factory_Bordered();
        }
    } 
    
    public static function getText()
    {
        $textBlock = self::getFactory()->createBlockText();
    	return $textBlock->render();
    }
    
    public static function getLine()
    {
        $lineBlock = self::getFactory()->createBlockLine();
        return $lineBlock->render();
    }
    
    public static function getContainer()
    {
        $containerBlock = self::getFactory()->createBlockContainer();
        return $containerBlock->render();
    }
}