<?php
class Pattern_Memento_Caretaker
{
	protected $_memento;
	
	public function setMemento(Pattern_Memento $memento)
	{
		$this->_memento = $memento;
	}
	
	public function getMemento()
	{
		return $this->_memento;
	}
}