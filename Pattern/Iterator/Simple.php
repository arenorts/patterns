<?php
class Pattern_Iterator_Simple implements Iterator
{
	protected $_iterableChildren;
	
	public function __construct(Pattern_Block_Abstract $iterable)
	{
		$this->_iterableChildren = $iterable->getChildren();
	}
	
	public function valid()
	{
		return ($this->key() !== null);
	}
	
	public function next()
	{
		return next($this->_iterableChildren);
	}
	
	public function current()
	{
		return current($this->_iterableChildren);
	}
	
	public function key() 
	{
		return key($this->_iterableChildren);
	}
	
	public function rewind() 
	{
		return reset($this->_iterableChildren);
	}
}