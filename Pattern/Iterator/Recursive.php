<?php
class Pattern_Iterator_Recursive implements Iterator
{
	protected $_iterableChildren;
	protected $_childIterator;
	
	public function __construct(Pattern_Block_Abstract $iterable)
	{
		$this->_iterableChildren = $iterable->getChildren();
	}
	
	public function valid()
	{
		return (($this->key() !== null) || (!empty($this->_childIterator) && $this->_childIterator->valid()));
	}
	
	public function next()
	{
		if (!empty($this->_childIterator)) {
			$this->_childIterator->next();
			if (!$this->_childIterator->valid()) {
				next($this->_iterableChildren);
				unset($this->_childIterator);
			}
		} else {
			$currentElement = current($this->_iterableChildren);
			
			$children = $currentElement->getChildren();
			if (!empty($children)) {
				$this->_childIterator = $currentElement->getIterator('recursive');
				$this->_childIterator->rewind();
			} else {
				next($this->_iterableChildren);
			}
		}
	}
	
	public function current()
	{
		if (!empty($this->_childIterator)) {
			return $this->_childIterator->current();
		} 
		return current($this->_iterableChildren);
	}
	
	public function key() 
	{
		return key($this->_iterableChildren);
	}
	
	public function rewind() 
	{
		return reset($this->_iterableChildren);
	}
}