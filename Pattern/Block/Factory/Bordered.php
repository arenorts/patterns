<?php
class Pattern_Block_Factory_Bordered extends Pattern_Block_Factory_Abstract
{
	public function getBaseElement()
	{
		/*return new Pattern_Block_Text();
		return new Pattern_Block_Text();
		return new Pattern_Block_Text();*/
	}
	
    public function createBlockText()
    {
        $text = new Pattern_Block_Text();
        return new Pattern_Decorator_Border($text);
    }

    public function createBlockLine()
    {
        $line = new Pattern_Block_Line();
        return new Pattern_Decorator_Border($line);
    }

    public function createBlockContainer()
    {
        $container = new Pattern_Block_Container();
        return new Pattern_Decorator_Border($container);
    }
}