<?php
abstract class Pattern_Block_Factory_Abstract
{
    abstract public function createBlockText();
    
    abstract public function createBlockLine();
    
    abstract public function createBlockContainer();
}