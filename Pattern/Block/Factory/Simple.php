<?php
class Pattern_Block_Factory_Simple extends Pattern_Block_Factory_Abstract
{
    public function createBlockText()
    {
        return new Pattern_Block_Text();
    }
    
    public function createBlockLine()
    {
        return new Pattern_Block_Line();
    }
    
    public function createBlockContainer()
    {
        return new Pattern_Block_Container();
    }
}