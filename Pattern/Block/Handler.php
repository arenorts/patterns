<?php
abstract class Pattern_Block_Handler
{
    abstract public function getAncestor();
    abstract public function handleHasPlugins();
    abstract public function handleGetAncestors();
    
    public function getParentWithPlugin()
    {
        $handler = $this;
        
        $result = $handler->handleHasPlugins();
        
        while ($result == null && $handler->getAncestor() != null) {
            $handler = $handler->getAncestor();
            $result = $handler->handleHasPlugins();
        }
        
        return $result;
    }
    
    public function getParentBlocks($maxlevel)
    {
        $handler = $this;
        
        $parents = array();
        $parents[] = $handler->handleGetAncestors();
        
        $i = 1;
        
        while ($i < $maxlevel && $handler->getAncestor() != null) {
            $handler = $handler->getAncestor();
            $parents[] = $handler->handleGetAncestors($this);
            $i++;
        }
        
        return $parents;
    }
}