<?php
abstract class Pattern_Block_Abstract 
    extends Pattern_Block_Handler
    implements Pattern_Observer, Pattern_Observable
{
	public $id;
	public $output;
	public $rendered = false;
	protected $_observers = array();
	
	protected $_cacheLifetime = false;
	
	public function getCacheLifeTime()
	{
	    return $this->_cacheLifetime;
	}
	
	public function setCacheLifetime($lifetime)
	{
	    $this->_cacheLifetime = $lifetime;
	    $this->fireEvent($lifetime);
	}
	
	public function __construct($params = null)
	{
		$this->id = (string) new MongoId();
		$this->_init($params);
	}
	
	protected function _init($params)
	{
	} 

/******************************************************************************
 * Observer
******************************************************************************/
	public function addObserver(Pattern_Observer $observer)
	{
	    $this->_observers[] = $observer;
	}
	
	public function fireEvent($lifetime) 
	{
	    foreach ($this->_observers as $observer) {
	        $observer->notify($lifetime);
	    }
	}
	
	public function notify($lifetime) 
	{
	    if ($this->hasPlugin('cache')) {
	        if ($this->getCacheLifeTime() > $lifetime) {
	            $this->setCacheLifeTime($lifetime);
	        }
	    }
	}
/******************************************************************************
 * Composite
******************************************************************************/
	
	protected $_ancestor;
	public $children = array();
	
	
	public function add(Pattern_Block_Abstract $block)
	{
		$block->setAncestor($this);
		$block->addObserver($this);
		$this->children[$block->id] = $block;
	}
	
	public function remove(Pattern_Block_Abstract $block)
	{
	    unset($this->children[$block->id]);
	}
	
	public function setAncestor(Pattern_Block_Abstract $ancestor)
	{
		$this->_ancestor = $ancestor;
	}
	
	public function getAncestor()
	{
	    return $this->_ancestor;
	}
	
	public function getChildren()
	{
		return $this->children;
	}
	
/******************************************************************************
 * Render
******************************************************************************/
	public function render() 
	{
		$this->output = '';
		$this->_pluginCall('preRender');
		
		if ($this->rendered) {
			return $this->output;
		}
		
		$this->output .= $this->_render();
		$this->_pluginCall('postRender');
		
		return $this->output;
	}
	
	abstract protected function _render();

/******************************************************************************
 * Iterator
******************************************************************************/
	
	protected $_iterators = array();
	protected $_plugins = array();
	
	public function getIterator($type = 'simple')
	{
		if (!isset($this->_iterators[$type])) {
			$className = 'Pattern_Iterator_' . ucfirst($type);
			$this->_iterators[$type] = new $className($this);
		}
		return $this->_iterators[$type];
	} 
	
	public function tree($level = 0)
	{
		$output = '';
		for ($i = 0; $i < $level; $i++) {
			$output .= '-';
		}
		$output .= get_class($this) . '<br>';
		foreach ($this->getChildren() as $child) {
			$output .= $child->tree($level + 1);
		}
		
		return $output;
	}

/******************************************************************************
 * Builder
******************************************************************************/
	public function getExport($type = 'text')
	{
	    $director = new Pattern_Block_Export_Director();
	    
	    if ($type == 'text') {
		    $exportBuilder = new Pattern_Block_Export_Text();
	    } elseif ($type == 'html') {
	        $exportBuilder = new Pattern_Block_Export_Html();
	    }
	    
	    $director->setBuilder($exportBuilder);
	    
	    $director->createExport($this);
	    
	    $export = $director->getExport();
	    return $export->getContent();
	}
	
/******************************************************************************
 * Strategy
******************************************************************************/
	
	protected function _pluginCall($methodName)
	{
		 foreach ($this->_plugins as $plugin) {
			if (method_exists($plugin, $methodName)) {
				$plugin->{$methodName}($this);
			}
		 } 
	}
	
	public function addPlugin($name, $params = array())
	{
		$className = 'Pattern_Plugin_' . ucfirst($name);
		$plugin = new $className($params);
		$this->_plugins[$name] = $plugin;
	}
	
	public function removePlugin($name)
	{
		unset($this->_plugins[$name]);
	}
	
	public function hasPlugin($pluginName)
	{
		return isset($this->_plugins[$pluginName]);
	}
	
/******************************************************************************
 * Chain
******************************************************************************/	
	
	public function hasPlugins()
	{
		if (!empty($this->_plugins)) {
			return $this;
		} else {
			if (!empty($this->_ancestor)) {
				return $this->_ancestor->hasPlugins();
			}
		}
		return false;
	}
	
	public function handleHasPlugins()
	{
	    if (!empty($this->_plugins)) {
	        return $this;
	    } else {
	        return null;
	    }
	}
	
	public function handleGetAncestors()
	{
	    return get_class($this->_ancestor);
	}
	

	
	public function getAncestors($maxlevel, $ancestors = array())
	{
		if (!empty($this->_ancestor) && sizeof($ancestors) < $maxlevel) {
			$ancestors[] = get_class($this->_ancestor);
			return $this->_ancestor->getAncestors($maxlevel, $ancestors);
		} else {
			return $ancestors;
		}
	}

/******************************************************************************
 * Visitor
******************************************************************************/
	
	public function accept($className)
	{
	    $visitor = Pattern_Visitor::getVisitor($className);
	    $visitor->visit($this);
	}
	
/******************************************************************************
 * End
******************************************************************************/
}