<?php
class Pattern_Block_Line extends Pattern_Block_Abstract
{
	protected function _render()
	{
		$output = '<hr>';
		foreach ($this->children as $child) {
				$output .= $child->render();
		}
		return $output;
	}
}