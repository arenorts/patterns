<?php
class Pattern_Block_Exchanges extends Pattern_Block_Abstract
{
    protected $_cacheLifetime = 100;
    
    public function __construct()
    {
        $this->addPlugin('cache');
    }
    
	protected function _render()
	{
	    if (!isset ($this->_content)) {
	        $this->_loadContent();
	    }
		return $this->_content;
	}
	
	protected function _loadContent()
	{
	    $this->_content = file_get_contents('https://www.liqpay.com/exchanges/exchanges.cgi');
	}
}