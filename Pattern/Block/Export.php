<?php
class Pattern_Block_Export
{
    protected $_content;
    
    public function getContent()
    {
        return $this->_content;
    }
    
	public function setPrefix($prefix = null)
	{
	    $this->_content = $prefix . $this->_content;
	}
	
	public function addTextBlock($element)
	{
	    $this->_content .= $element;
	}
	
	public function addLineBlock($element)
	{
	    $this->_content .= $element;
	}
	
	public function addContainerBlock($element)
	{
	    $this->_content .= $element;
	}

	public function setPostfix($postfix = null)
	{
	    $this->_content .= isset($postfix) ? $postfix : $this->_defaultPostfix;
	}
}