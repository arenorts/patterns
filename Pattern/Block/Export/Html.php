<?php
class Pattern_Block_Export_Html extends Pattern_Block_Export_Abstract
{
    protected $_defaultPrefix = '<h2> HTML export: </h2> <br> <div style="border:1px solid #000;">';
    protected $_defaultPostfix = '</div>';
    
	public function setPrefix($prefix = null)
	{
	    $this->_export->setPrefix(isset($prefix) ? $prefix : $this->_defaultPrefix);
	}
	
	public function addTextBlock(Pattern_Block_Abstract $block)
	{
	    $element = '<div style="color:red;">' . get_class($block) . '</div>';
	    $this->_export->addTextBlock($element);
	}
	
	public function addLineBlock(Pattern_Block_Abstract $block)
	{
	    $element = '<div style="color:green;">' . get_class($block) . '</div>';
	    $this->_export->addLineBlock($element);
	}
	
	public function addContainerBlock(Pattern_Block_Abstract $block)
	{
	    $element = '<div style="color:blue;">' . get_class($block) . '</div>';
	    $this->_export->addContainerBlock($element);
	}

	public function setPostfix($postfix = null)
	{
	    $this->_export->setPostfix(isset($postfix) ? $postfix : $this->_defaultPostfix);
	}
}