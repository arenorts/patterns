<?php
abstract class Pattern_Block_Export_Abstract
{
    protected $_defaultPrefix;
    protected $_defaultPostfix;
	protected $_export;
	
	public function getResult() 
	{
	    return $this->_export;
	}
	
	public function createExport()
	{
	    $this->_export = new Pattern_Block_Export(); 
	}
	
	abstract public function setPrefix($prefix = null);
	abstract public function addTextBlock(Pattern_Block_Abstract $block);
	abstract public function addLineBlock(Pattern_Block_Abstract $block);
	abstract public function addContainerBlock(Pattern_Block_Abstract $block);
	abstract public function setPostfix($postfix = null);
}