<?php
class Pattern_Block_Export_Director
{
	protected $_builder;
	
	public function setBuilder(Pattern_Block_Export_Abstract $builder)
	{
		$this->_builder = $builder;
	}
	
	public function getExport()
	{
	    return $this->_builder->getResult();
	}
	
	public function createExport(Pattern_Block_Abstract $block)
	{
	    $this->_builder->createExport();
	    $this->_builder->setPrefix();
	    
	    $iterable = $block->getIterator('recursive');
	    
	    foreach ($iterable as $element) {
	        if ($element instanceof Pattern_Block_Text) {
	            $this->_builder->addTextBlock($element);
	        } elseif ($element instanceof Pattern_Block_Line) {
	            $this->_builder->addLineBlock($element);
	        } elseif ($element instanceof Pattern_Block_Container) {
	            $this->_builder->addContainerBlock($element);
	        }
	    }
	    
	    $this->_builder->setPostfix();
	}
}