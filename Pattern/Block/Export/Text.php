<?php
class Pattern_Block_Export_Text extends Pattern_Block_Export_Abstract
{
    protected $_defaultPrefix = 'Simple text export:';
    protected $_defaultPostfix = ' end;';
	
	public function setPrefix($prefix = null)
	{
	    $this->_export->setPrefix(isset($prefix) ? $prefix : $this->_defaultPrefix);
	}
	
	public function addTextBlock(Pattern_Block_Abstract $block)
	{
	    $element = ':TEXT--' . get_class($block) . ':';
	    $this->_export->addTextBlock($element);
	}
	
	public function addLineBlock(Pattern_Block_Abstract $block)
	{
	    $element = ':LINE--' . get_class($block) . ':';
	    $this->_export->addLineBlock($element);
	}
	
	public function addContainerBlock(Pattern_Block_Abstract $block)
	{
	    $element = ':CONTAINER--' . get_class($block) . ':';
	    $this->_export->addContainerBlock($element);
	}

	public function setPostfix($postfix = null)
	{
	    $this->_export->setPostfix(isset($postfix) ? $postfix : $this->_defaultPostfix);
	}
}