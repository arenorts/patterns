<?php
class Pattern_Block_Container extends Pattern_Block_Abstract
{
	protected $_color = '#000';
	
	protected function _init($color = '#000')
	{
		$this->_color = $color;
	}
	
	protected function _render()
	{
		$output = '<div style="border:3px solid '.$this->_color.'">';
		foreach ($this->children as $child) {
			$output .= $child->render();
		}
		$output .= '</div>';
		
		return $output;
	}
}