<?php
class Pattern_Block_Text extends Pattern_Block_Abstract implements Pattern_Mediator_Collegue
{
	protected $_text;
	
	protected function _init($text = 'text')
	{
		$this->_text = $text;	
	}
	
	protected function _render()
	{
		$output = $this->_text;
		foreach ($this->children as $child) {
			$output .= $child->render();
		}
		return $output;
	}
	
	public function getValue()
	{
	    return $this->_text;
	}
	
	public function setValue($value)
	{
	    $this->_text = $value;
	}
}