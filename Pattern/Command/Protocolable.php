<?php
abstract class Pattern_Command_Protocolable extends Pattern_Command_Abstract
{
    protected $_logger;
    
    public function __construct($loggertype = Pattern_Command_Logger::T_FILE)
    {
        $this->_logger =  Pattern_Command_Logger::factory($loggertype);
    }
    
    public function getLogger()
    {
        return $this->_logger;
    }
    
	public function execute()
	{
		$result = $this->_execute();
		Pattern_Command_History::getInstance()->addExecuted($this);
		$this->getLogger()->log('Executed result: ' . serialize($result));
		return $result;
	}
	
	abstract protected function _execute();

	public function cancel() 
	{
		$result = $this->_cancel();
		Pattern_Command_History::getInstance()->addCancelled($this);
		$this->getLogger()->log('Cancelled result: ' . serialize($result));
		return $result;
	}
	
	abstract protected function _cancel();
}