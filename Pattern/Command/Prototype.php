<?php
class Pattern_Command_Prototype
{
	protected $_command;
	
	public function __construct(Pattern_Command_Abstract $command)
	{
		$this->_command = $command;
	}
	
	public function getCommand()
	{
		return clone $this->_command;
	}
}