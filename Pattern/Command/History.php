<?php
class Pattern_Command_History
{
	protected static $_instance;
	protected static $_executed = array();
	protected static $_cancelled = array();
	
	private function __construct()
	{
	}
	
	public static function getInstance()
	{
		if (!isset(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function addExecuted(Pattern_Command_Protocolable $command)
	{
		$prototype = new Pattern_Command_Prototype($command);
		array_push(self::$_executed, $prototype->getCommand());
		$this->_clearCancelled();
	}
	
	public function addCancelled(Pattern_Command_Protocolable $command)
	{
		$prototype = new Pattern_Command_Prototype($command);
		array_push(self::$_cancelled, $prototype->getCommand());
	}
	
	protected function _clearCancelled()
	{
	    self::$_cancelled = array();
	}
	
	public function getLastExecuted()
	{
		return array_pop(self::$_executed);
	}
	
	public function getLastCancelled()
	{
		return array_pop(self::$_cancelled);
	}
}