<?php
class Pattern_Command_Redo extends Pattern_Command_Abstract
{
	public function execute()
	{
		return Pattern_Command_History::getInstance()->getLastCancelled()->execute();
	}
}