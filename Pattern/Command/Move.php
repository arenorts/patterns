<?php
class Pattern_Command_Move extends Pattern_Command_Protocolable
{
	protected $_block;
	protected $_source;
	protected $_dest;

	public function __construct(&$block, &$source, &$dest)
	{
	    parent::__construct();
		$this->_block = $block;
		$this->_source = $source;
		$this->_dest = $dest;
	}

	protected function _execute()
	{
	    $this->_block->setAncestor($this->_dest);
	    $this->_source->remove($this->_block);
	    $this->_dest->add($this->_block);
	}

	protected function _cancel()
	{
	    $this->_block->setAncestor($this->_source);
	    $this->_dest->remove($this->_block);
	    $this->_source->add($this->_block);
	}
}