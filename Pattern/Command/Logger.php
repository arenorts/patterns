<?php
class Pattern_Command_Logger
{
    const T_FILE   = 'file';
    const T_DB     = 'db';
    
    public static function getClass($type, $throwEx = false)
    {
        $type = ucfirst($type);
        $class = 'Pattern_Command_Logger_' . ucfirst($type);
    
        if (!class_exists($class) && $throwEx === true) {
            throw new Exception('Invalid logger type - ' . $type);
        }
    
        return $class;
    }
    
    
    public static function factory($type, array $params = null)
    {
        $class = self::getClass($type, true);
        $message = new $class();
    
        if ($message instanceof Pattern_Command_Logger_Abstract) {
            return $message;
        } else {
            throw new Exception('Logger must be instance of Pattern_Command_Logger_Abstract');
        }
    }
}