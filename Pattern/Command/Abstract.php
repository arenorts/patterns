<?php
abstract class Pattern_Command_Abstract
{
	abstract public function execute();
	
	public function save()
	{
		$prototype = new Pattern_Command_Prototype($this);
		return new Pattern_Memento($this);
	}
}