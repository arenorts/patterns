<?php
class Pattern_Command_Logger_Db extends Pattern_Command_Logger_Abstract
{
    public function log($message)
    {
        $entry = new Pattern_Command_Logger_Db_Entry();
        $entry->setData('message', $message);
        $entry->setData('created', time());
        $entry->save();
    }
}