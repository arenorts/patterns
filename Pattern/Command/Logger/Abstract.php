<?php
abstract class Pattern_Command_Logger_Abstract
{
    abstract public function log($message);
}