<?php
class Pattern_Command_Logger_File extends Pattern_Command_Logger_Abstract
{
    public function log($message)
    {
        Log::write('info', $message, __METHOD__);
    }
}