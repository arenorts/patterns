<?php
class Pattern_Command_Undo extends Pattern_Command_Abstract
{
	public function execute()
	{
		return Pattern_Command_History::getInstance()->getLastExecuted()->cancel();
	}
}