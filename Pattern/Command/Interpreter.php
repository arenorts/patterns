<?php
class Pattern_Command_Interpreter
{
    protected $_syntax = array(
        '+plugin' => '_addPlugin', 
        '->'      => '_move',
    );
    
    public function interpret($command, $params) 
    {
        if (!isset($this->_syntax[$command])) {
            throw new Exception('Invalid command');
        }
        
        $method = $this->_syntax[$command];
        $result = $this->{$method}($params);
        
        return $result;
    }
    
    protected function _addPlugin($params)
    {
        $command = new Pattern_Command_Add_Plugin($params['block'], $params['pluginName']);
        return $command->execute();
    }
    
    protected function _move($params)
    {
        $command = new Pattern_Command_Move($params['block'], $params['source'], $params['destination']);
        return $command->execute();
    }
}