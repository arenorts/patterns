<?php
class Pattern_Command_Add_Plugin extends Pattern_Command_Protocolable
{
	protected $_block;
	protected $_pluginName;
	protected $_state;
	
	public function __construct($block, $pluginName)
	{
	    parent::__construct();
	    $this->_state = new Pattern_Command_Add_Plugin_State_New($block, $pluginName);
		$this->_block = $block;
		$this->_pluginName = $pluginName;
	}
	
	protected function _execute()
	{
		$result = $this->_state->execute();
		$this->_state = new Pattern_Command_Add_Plugin_State_Executed($this->_block, $this->_pluginName);
		return $result;
	}
	
	protected function _cancel()
	{
		$result = $this->_state->cancel();
		$this->_state = new Pattern_Command_Add_Plugin_State_Cancelled($this->_block, $this->_pluginName);
		return $result;
	}
}