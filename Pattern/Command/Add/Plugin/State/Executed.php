<?php
class Pattern_Command_Add_Plugin_State_Executed extends Pattern_Command_Add_Plugin_State_Abstract
{
    public function execute()
	{
		throw new Exception('Can\'t execute executed command');
	}
	
	public function cancel()
	{
		$this->_block->removePlugin($this->_pluginName);
		return $this->_block;
	}
}