<?php
abstract class Pattern_Command_Add_Plugin_State_Abstract
{
    protected $_block;
    protected $_pluginName;
    
    public function __construct($block, $pluginName)
    {
        $this->_block = $block;
        $this->_pluginName = $pluginName;
    }
    
    abstract public function execute();
    
    abstract public function cancel();
}