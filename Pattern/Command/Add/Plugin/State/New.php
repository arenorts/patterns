<?php
class Pattern_Command_Add_Plugin_State_New extends Pattern_Command_Add_Plugin_State_Abstract
{
	public function execute()
	{
		$this->_block->addPlugin($this->_pluginName);
		return $this->_block;
	}
	
	public function cancel()
	{
		throw new Exception('Can\'t cancel not executed command');
	}
}