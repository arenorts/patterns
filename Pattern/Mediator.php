<?php
class Pattern_Mediator
{
    protected $_collegues = array();
    
    public function addCollegue(Pattern_Mediator_Collegue &$collegue)
    {
        $this->_collegues[] = $collegue;
    }
    
    public function mix()
    {
        $keys = array_rand($this->_collegues, rand(1, count($this->_collegues)));
        
        $newValue = '';
        
        foreach ((array) $keys as $key) {
            $newValue .= $this->_collegues[$key]->getValue();
        }
        
        end($this->_collegues)->setValue($newValue);
    }
}