<?php
class Pattern_Visitor_Cache
{
	public function visit(Pattern_Block_Abstract $block) 
	{
	    if (!$block->hasPlugin('cache')) {
	        $block->addPlugin('cache');
	        $block->setCacheLifetime($this->_determineLifetime($block));
	    }
	} 

	protected function _determineLifetime($block)
	{
	    if ($block instanceof Pattern_Block_Text) {
	        return 100;
	    } elseif ($block instanceof Pattern_Block_Line) {
	        return 10000;
	    } elseif ($block instanceof Pattern_Block_Container) {
	        return 50;
	    }
	}
}