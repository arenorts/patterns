<?php
abstract class Pattern_Visitor_Abstract
{
    abstract public function visit(Pattern_Block_Abstract $block);
}