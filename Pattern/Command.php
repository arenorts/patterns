<?php
abstract class Pattern_Command
{
	abstract public function execute();
	
	abstract public function cancel();
}