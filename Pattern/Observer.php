<?php
interface Pattern_Observer
{
    public function notify($lifetime);
}