<?php
abstract class Pattern_Plugin_Abstract
{
    abstract public function preRender($block);

    abstract public function postRender($block);
}