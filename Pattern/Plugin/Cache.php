<?php
class Pattern_Plugin_Cache extends Pattern_Plugin_Abstract
{
	public function preRender($block)
	{
		$cache = Zend_Registry::get('coreCache');
		$id = 'plugin_cache' . md5(serialize($block->tree()));
		
		$content = $cache->load($id);
		if ($content) {
			$block->output .= 'From cache: <br>' . $content;
			
			$block->rendered = true;
		}
	}
	
	public function postRender($block)
	{
		$cache = Zend_Registry::get('coreCache');
		$id = 'plugin_cache' . md5(serialize($block->tree()));
		$cache->save($block->output, $id, array(), $block->getCacheLifeTime());
	}
}