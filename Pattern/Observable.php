<?php
interface Pattern_Observable
{
    public function addObserver(Pattern_Observer $observer);
    public function fireEvent($lifetime);
}