<?php
class Pattern_Visitor
{
    protected static $_pull = array();
    
    public static function getVisitor($class)
    {
        if (isset(self::$_pull[$class])) {
            return self::$_pull[$class];
        } else {
            $visitor = new $class();
            self::$_pull[$class] = $visitor;
            return $visitor;
        }
    }
    
}