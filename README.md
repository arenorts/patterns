# README #

Main start file - Pattern/Block/Abstract.php

### Creational patterns ###

* Abstract factory
* Builder
* Factory method
* Singleton
* Prototype

### Structural patterns ###

* Adapter / Wrapper
* Composite
* Decorator
* Facade

### Behavioral patterns ###

* Chain of responsibility
* Command
* Interpreter
* Iterator
* Mediator
* Memento
* Observer
* State
* Strategy
* Template method
* Visitor